# **Metronome AOC**
____________________________________________________________________________

**Aleddine BAGHDADI & Salma BOURI** 

**M2-GLA**

____________________________________________________________________________

A metronome is a device that makes regular beats at a steady tempo which helps for marking rhythm, especially in practicing music.

The goal of this project is to develop and design an implementation of a metronome in Java using command, observer and adapter patterns.