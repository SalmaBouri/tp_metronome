package fr.istic.aoc.tp_metronome.Molette;

import fr.istic.aoc.tp_metronome.view.IView;

/**
 * Created by alaeddine on 05/01/17.
 */
public class MoletteImpl implements  Molette {

    IView view;

    public MoletteImpl(IView view) {
        this.view = view;
    }

    @Override
    public float position() {
        return (float)this.view.getSlider().getValue();
    }
}
