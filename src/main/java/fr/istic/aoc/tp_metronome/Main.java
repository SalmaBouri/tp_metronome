package fr.istic.aoc.tp_metronome;

import java.io.IOException;

import fr.istic.aoc.tp_metronome.commande.*;
import fr.istic.aoc.tp_metronome.controleur.ControleurImpl;
import fr.istic.aoc.tp_metronome.controleur.IControleur;
import fr.istic.aoc.tp_metronome.Moteur.IMoteur;
import fr.istic.aoc.tp_metronome.Moteur.Moteur;
import fr.istic.aoc.tp_metronome.view.IView;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
    private Stage primaryStage;
    private BorderPane rootLayout;
    public IView view;
    public IMoteur moteur;
    public IControleur controleur;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("MetronomApp");

        initRootLayout();

        showVmetronome();
    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);


            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the metronome overview inside the root layout.
     */
    public void showVmetronome() {
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Vmetronome.fxml"));
            AnchorPane vMetronome = (AnchorPane) loader.load();

            view = loader.getController();

            moteur = new Moteur();
            controleur= new ControleurImpl(moteur, view);

            Commande cmdMarquerTemps = new MarquerTemps(controleur);
            Commande cmdMarquerMesure = new MarquerMesure(controleur);
            Commande cmdStart = new Start(controleur);
            Commande cmdStop = new Stop(controleur);
            Commande cmdInc = new Inc(controleur);
            Commande cmdTempo = new Tempo(controleur);
            Commande cmdSonner = new Sonner(controleur);
            Commande cmdDec = new Dec(controleur);

            moteur.setCmdMarquerTemps(cmdMarquerTemps);
            moteur.setCmdSonner(cmdSonner);
            moteur.setCmdMarquerMesure(cmdMarquerMesure);

            view.setCommandeStart(cmdStart);
            view.setCommandeStop(cmdStop);
            view.setCommandeTempo(cmdTempo);
            view.setCommandeInc(cmdInc);
            view.setCommandeDec(cmdDec);


            rootLayout.setCenter(vMetronome);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
