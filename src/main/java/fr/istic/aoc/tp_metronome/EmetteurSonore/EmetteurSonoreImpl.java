package fr.istic.aoc.tp_metronome.EmetteurSonore;

import javafx.scene.media.AudioClip;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by alaeddine on 05/01/17.
 */
public class EmetteurSonoreImpl implements EmetteurSonore {
    @Override
    public void emettreClic() {

        String url    = "file:./src/main/resources/SONNER.wav";

        AudioClip audio = null;

        try {
            audio = new AudioClip(new URL(url).toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        audio.play();

    }
}
