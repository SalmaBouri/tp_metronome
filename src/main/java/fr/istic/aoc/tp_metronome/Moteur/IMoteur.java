package fr.istic.aoc.tp_metronome.Moteur;

import fr.istic.aoc.tp_metronome.commande.Commande;

public interface IMoteur {

    Commande getCmdSonner();

    void setCmdSonner(Commande cmdSonner);

    Boolean getEnMode();

    void setEnMode(Boolean enMode);

    float getTempo();

    void setTempo(float tempo);

    int getNbTpm();

    void setNbTpm(int nbTpm);

    Commande getCmdMarquerTemps();

    void setCmdMarquerTemps(Commande cmdMarquerTemps);

    Commande getCmdMarquerMesure();

    void setCmdMarquerMesure(Commande cmdMarquerMesure);

    void cmdMarquerTemps();

    void cmdMarquerMesure();

}
