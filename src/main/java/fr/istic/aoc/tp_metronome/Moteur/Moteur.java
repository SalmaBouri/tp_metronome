package fr.istic.aoc.tp_metronome.Moteur;

import fr.istic.aoc.tp_metronome.Horloge.Horloge;
import fr.istic.aoc.tp_metronome.Horloge.HorlogeImpl;
import fr.istic.aoc.tp_metronome.commande.Commande;

public class Moteur implements IMoteur {

    private Boolean enMode;
    private float tempo;
    private int nbTpm;
    private Commande cmdSonner;
    private Commande cmdMarquerTemps;
    private Commande cmdMarquerMesure;
    private Horloge horloge;

    public Moteur() {
        this.enMode = false;
        this.tempo = 50;
        this.nbTpm = 4;
    }

    public Boolean getEnMode() {
        return enMode;
    }

    public void setEnMode(Boolean enMode) {
        this.enMode = enMode;
        if (this.enMode) {

            if (this.horloge == null) {
                this.horloge = new HorlogeImpl();
            }
            System.out.println("ON");

            this.horloge.activerPeriodiquement(cmdSonner, 60 / tempo);
            this.horloge.activerPeriodiquement(cmdMarquerTemps, 60 / tempo);
            this.horloge.activerPeriodiquement(cmdMarquerMesure, (60 / tempo) * nbTpm);

        } else {
            this.horloge.desactiver(cmdSonner);
            this.horloge.desactiver(cmdMarquerTemps);
            this.horloge.desactiver(cmdMarquerMesure);
        }


    }

    public Commande getCmdSonner() {
        return cmdSonner;
    }

    public void setCmdSonner(Commande cmdSonner) {
        this.cmdSonner = cmdSonner;
    }

    public float getTempo() {
        return tempo;
    }

    public void setTempo(float tempo) {
        this.tempo = tempo;
    }

    public int getNbTpm() {
        return nbTpm;
    }

    public void setNbTpm(int nbTpm) {
        this.nbTpm = nbTpm;
    }

    public Horloge getHorloge() {
        return horloge;
    }

    public void setHorloge(Horloge horloge) {
        this.horloge = horloge;
    }

    public Commande getCmdMarquerTemps() {
        return cmdMarquerTemps;
    }

    public void setCmdMarquerTemps(Commande cmdMarquerTemps) {
        this.cmdMarquerTemps = cmdMarquerTemps;
    }

    public Commande getCmdMarquerMesure() {
        return cmdMarquerMesure;
    }

    public void setCmdMarquerMesure(Commande cmdMarquerMesure) {
        this.cmdMarquerMesure = cmdMarquerMesure;
    }

    public void cmdMarquerTemps() {
        this.cmdMarquerMesure.execute();
    }

    public void cmdMarquerMesure() {
        this.cmdMarquerMesure.execute();
    }
}
