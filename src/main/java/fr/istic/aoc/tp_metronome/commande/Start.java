package fr.istic.aoc.tp_metronome.commande;


import fr.istic.aoc.tp_metronome.controleur.IControleur;

public class Start implements Commande{

	IControleur controleur;

	public Start(IControleur controleur) {
		super();
		this.controleur = controleur;
		System.out.println("STARTT");
	}
	public void execute() {

		controleur.start();

	}
}
