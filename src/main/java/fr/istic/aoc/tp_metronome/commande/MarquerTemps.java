package fr.istic.aoc.tp_metronome.commande;

import fr.istic.aoc.tp_metronome.controleur.IControleur;

public class MarquerTemps implements Commande{

	IControleur controleur;

	public MarquerTemps(IControleur controleur) {
		super();
		this.controleur = controleur;
	}

	public void execute() {
		this.controleur.marquerTemps();
	}

}
