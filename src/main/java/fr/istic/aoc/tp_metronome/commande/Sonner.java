package fr.istic.aoc.tp_metronome.commande;

import fr.istic.aoc.tp_metronome.controleur.IControleur;

/**
 * Created by alaeddine on 05/01/17.
 */
public class Sonner implements Commande {
    IControleur controleur;

    public Sonner(IControleur controleur) {
        super();
        this.controleur = controleur;
    }

    @Override
    public void execute() {
        this.controleur.sonner();
    }
}
