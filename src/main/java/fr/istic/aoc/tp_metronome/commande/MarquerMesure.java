package fr.istic.aoc.tp_metronome.commande;

import fr.istic.aoc.tp_metronome.controleur.IControleur;

public class MarquerMesure implements Commande{

	IControleur controleur;

	public MarquerMesure(IControleur controleur) {
		super();
		this.controleur = controleur;
	}

	public void execute() {
		this.controleur.marquerMesure();
	}

}
