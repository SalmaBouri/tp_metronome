package fr.istic.aoc.tp_metronome.commande;


import fr.istic.aoc.tp_metronome.controleur.IControleur;

public class Stop implements Commande{

	IControleur controleur;

	public Stop(IControleur controleur) {
		super();
		this.controleur = controleur;
	}

	public void execute() {
		controleur.stop();
	}

}
