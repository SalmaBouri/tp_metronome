package fr.istic.aoc.tp_metronome.commande;

public interface Commande {
	void execute();
}
