package fr.istic.aoc.tp_metronome.commande;

import fr.istic.aoc.tp_metronome.controleur.IControleur;

public class Inc implements Commande{

	IControleur controleur;

	public Inc(IControleur controleur) {
		super();
		this.controleur = controleur;
	}
	public void execute() {
		controleur.inc();


	}

}
