package fr.istic.aoc.tp_metronome.commande;

import fr.istic.aoc.tp_metronome.controleur.IControleur;

public class Dec implements Commande{

	private IControleur controleur;

	public Dec(IControleur controleur) {
		super();
		this.controleur = controleur;
	}

	public void execute() {
		controleur.dec();
	}

}
