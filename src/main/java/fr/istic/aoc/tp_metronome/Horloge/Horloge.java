package fr.istic.aoc.tp_metronome.Horloge;

import fr.istic.aoc.tp_metronome.commande.Commande;

public interface Horloge {

	// Appel périodique de l’opération execute() de cmd,
	// toutes les périodeEnSecondes secondes,
	// avec une précision d’une milliseconde.
	void activerPeriodiquement(Commande cmd,float periodeEnSecondes);

	// Appel de l’opération execute() de cmd,
	// après un délai de délaiEnSecondes secondes,
	// avec une précision d’une milliseconde.
	void activerApresDelai(Commande cmd, float delaiEnSecondes);

	void desactiver(Commande cmd);

}
