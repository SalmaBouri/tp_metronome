package fr.istic.aoc.tp_metronome.Horloge;

import fr.istic.aoc.tp_metronome.Horloge.Horloge;
import fr.istic.aoc.tp_metronome.commande.Commande;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class HorlogeImpl implements Horloge {


    private List<CommandeTimer> tasks = new ArrayList<CommandeTimer>();


    public void activerPeriodiquement(Commande cmd, float periodeEnSecondes) {

        CommandeTimer task = new CommandeTimer(cmd,(int)(periodeEnSecondes*1000));
        task.start();
        tasks.add(task);
    }

    public void activerApresDelai(Commande cmd, float delaiEnSecondes) {
        try{
            Thread.sleep((int)(delaiEnSecondes*1000));
            cmd.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void desactiver(Commande cmd) {
        for(int i = 0; i<tasks.size(); i++){
            if(tasks.get(i).getCommande().equals(cmd)){
                tasks.get(i).stop();
                tasks.remove(i);
            }
        }

    }


    private class CommandeTimer{

        private Commande commande;
        private int msPeriode;
        private Timer timer = new Timer();

        public CommandeTimer(Commande commande, int msPeriode){
            this.commande = commande;
            this.msPeriode = msPeriode;
        }

        public Commande getCommande(){
            return commande;
        }

        public void start(){

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    commande.execute();
                }
            };
            timer.scheduleAtFixedRate(task, 0, msPeriode);
        }
        public void stop(){
            timer.cancel();
        }
    }

}
