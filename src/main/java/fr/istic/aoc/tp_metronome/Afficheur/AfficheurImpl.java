package fr.istic.aoc.tp_metronome.Afficheur;

import fr.istic.aoc.tp_metronome.view.IView;
import javafx.application.Platform;
import javafx.scene.paint.Color;

/**
 * Created by alaeddine on 04/01/17.
 */
public class AfficheurImpl implements Afficheur {

    IView view;

    public AfficheurImpl(IView view) {
        this.view = view;
    }

    @Override
    public void allumerLED(int numLED) {
        if(numLED==0){
            Platform.runLater(new Runnable() {
                public void run() {
                    view.getLed1().setFill(Color.GREEN);
                }
            });
        }
        else if(numLED==1){
            Platform.runLater(new Runnable() {
                public void run() {
                    view.getLed2().setFill(Color.BLUE);
                }
            });
        }
    }

    @Override
    public void eteindreLED(int numLED) {
        if(numLED==0){
            Platform.runLater(new Runnable() {
                public void run() {
                    view.getLed1().setFill(Color.WHITE);
                }
            });
        }
        else if(numLED==1){
            Platform.runLater(new Runnable() {
                public void run() {
                    view.getLed2().setFill(Color.WHITE);
                }
            });
        }
    }

    @Override
    public void afficherTempo(int valeurTempo) {
        view.setTempo(valeurTempo);
    }

    @Override
    public void afficherMesure(int valeurMesure) {
        view.setMesure(valeurMesure);
    }
}
