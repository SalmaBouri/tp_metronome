package fr.istic.aoc.tp_metronome.controleur;

public interface IControleur {

	void marquerTemps();
	void start();
	void stop();
	void marquerMesure();
	void updateTempo();
	void dec();
	void sonner();
	void inc();

}
