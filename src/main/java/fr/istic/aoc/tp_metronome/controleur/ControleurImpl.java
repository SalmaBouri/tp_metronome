package fr.istic.aoc.tp_metronome.controleur;

import java.util.Observable;
import java.util.Observer;

import fr.istic.aoc.tp_metronome.Afficheur.Afficheur;
import fr.istic.aoc.tp_metronome.Afficheur.AfficheurImpl;
import fr.istic.aoc.tp_metronome.EmetteurSonore.EmetteurSonore;
import fr.istic.aoc.tp_metronome.EmetteurSonore.EmetteurSonoreImpl;
import fr.istic.aoc.tp_metronome.Moteur.IMoteur;
import fr.istic.aoc.tp_metronome.view.IView;

public class ControleurImpl implements IControleur {

	private IMoteur moteur;
	private IView view;
	private Afficheur afficheur;
	private EmetteurSonore emetteurSonore;

	public ControleurImpl(IMoteur moteur, IView view) {
		this.moteur = moteur;
		this.view = view;
		afficheur = new AfficheurImpl(view);
		emetteurSonore = new EmetteurSonoreImpl();
		afficheur.afficherMesure(this.moteur.getNbTpm());
		afficheur.afficherTempo((int) this.moteur.getTempo());
	}

	public IMoteur getMoteur() {
		return moteur;
	}

	public void setMoteur(IMoteur moteur) {
		this.moteur = moteur;
	}

	public IView getView() {
		return view;
	}

	public void setView(IView view) {
		this.view = view;
	}

	public void marquerTemps() {
		System.out.println("marquerTemps");
		this.afficheur.allumerLED(0);
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.afficheur.eteindreLED(0);
	}

	public void marquerMesure() {
		System.out.println("marquerMesure");
		this.afficheur.allumerLED(1);
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.afficheur.eteindreLED(1);

	}

	public void start() {
		if(!moteur.getEnMode())
			moteur.setEnMode(true);

	}

	public void stop() {
		if(moteur.getEnMode())
			moteur.setEnMode(false);

	}

	public void dec() {
		int nbTpm = this.moteur.getNbTpm();
		if(nbTpm>2 && nbTpm<=7){
			this.moteur.setNbTpm(nbTpm-1);
			this.afficheur.afficherMesure(nbTpm-1);
			if(moteur.getEnMode()){
				stop();
				start();
			}
		}

	}

	public void sonner(){
		emetteurSonore.emettreClic();
	}

	public void inc() {
		int nbTpm = this.moteur.getNbTpm();
		if(nbTpm>=2 && nbTpm<7){
			this.moteur.setNbTpm(nbTpm+1);
			this.afficheur.afficherMesure(nbTpm+1);
			if(moteur.getEnMode()){
				stop();
				start();
			}
		}

	}

	public void updateTempo(){
		System.out.println(this.view.getSlider().getValue());
		float tempo = (float)this.view.getSlider().getValue();
		this.moteur.setTempo(tempo);
		this.view.setTempo(tempo);
		if(moteur.getEnMode()){
			stop();
			start();
		}
	}

}
