package fr.istic.aoc.tp_metronome.view;

import java.util.Observer;

import fr.istic.aoc.tp_metronome.commande.Commande;
import fr.istic.aoc.tp_metronome.controleur.IControleur;
import javafx.scene.control.Slider;
import javafx.scene.shape.Circle;

public interface IView {


    Commande getCommandeStart();

    void setCommandeStart(Commande commandeStart);

    Commande getCommandeStop();

    void setCommandeStop(Commande commandeStop);

    Commande getCommandeInc();

    void setCommandeInc(Commande commandeInc);

    Commande getCommandeTempo();

    void setCommandeTempo(Commande commandeTempo);

    Commande getCommandeDec();

    void setCommandeDec(Commande commandeDec);

    Circle getLed1();

    void setLed1(Circle led1);

    Circle getLed2();

    void setLed2(Circle led2);

    Slider getSlider();

    void setSlider(Slider slider);

    void setMesure(float mesure);

    void setTempo(float tempo);

}
