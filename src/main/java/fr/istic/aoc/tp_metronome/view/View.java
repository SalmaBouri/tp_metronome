package fr.istic.aoc.tp_metronome.view;


import fr.istic.aoc.tp_metronome.commande.Commande;
import fr.istic.aoc.tp_metronome.controleur.IControleur;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;

public class View implements IView {

    private Commande commandeStart;
    private Commande commandeStop;
    private Commande commandeInc;
    private Commande commandeDec;
    private Commande commandeTempo;

    @FXML
    private Circle led1;

    @FXML
    private Circle led2;

    @FXML
    private TextField tempo;

    @FXML
    private TextField mesure;

    @FXML
    private Slider slider;

    public Commande getCommandeTempo() {
        return commandeTempo;
    }

    public void setCommandeTempo(Commande commandeTempo) {
        this.commandeTempo = commandeTempo;
    }

    public Commande getCommandeStart() {
        return commandeStart;
    }

    public void setCommandeStart(Commande commandeStart) {
        this.commandeStart = commandeStart;
    }

    public Commande getCommandeStop() {
        return commandeStop;
    }

    public void setCommandeStop(Commande commandeStop) {
        this.commandeStop = commandeStop;
    }

    public Commande getCommandeInc() {
        return commandeInc;
    }

    public void setCommandeInc(Commande commandeInc) {
        this.commandeInc = commandeInc;
    }

    public Commande getCommandeDec() {
        return commandeDec;
    }

    public void setCommandeDec(Commande commandeDec) {
        this.commandeDec = commandeDec;
    }

    @Override
    public Circle getLed1() {
        return led1;
    }

    @Override
    public void setLed1(Circle led1) {
        this.led1 = led1;
    }

    @Override
    public Circle getLed2() {
        return led2;
    }

    @Override
    public void setLed2(Circle led2) {
        this.led2 = led2;
    }

    @FXML
    private void buttonStart() {
        if (this.commandeStart != null)
            this.commandeStart.execute();
    }

    @FXML
    private void buttonStop() {
        if (this.commandeStop != null)
            this.commandeStop.execute();
    }

    @FXML
    private void buttonInc() {
        if (this.commandeStart != null)
            this.commandeInc.execute();
    }

    @FXML
    private void buttonDec() {
        if (this.commandeStop != null)
            this.commandeDec.execute();
    }

    @FXML
    private void mouseSliderChanged() {
        this.commandeTempo
                .execute();
    }

    public void setTempo(float tempo) {
        this.tempo.setText(String.valueOf(tempo));
        this.slider.setValue(tempo);
    }

    public void setMesure(float mesure) {
        this.mesure.setText(String.valueOf(mesure));
    }


    public Slider getSlider() {
        return slider;
    }

    public void setSlider(Slider slider) {
        this.slider = slider;
    }

}
